#! /usr/bin/env python
#  Copyright (C) 2014  Sebastian Garcia
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Author:
# Sebastian Garcia, eldraco@gmail.com
#
# Changelog

# Description
#


# standard imports
import getopt
import sys
import re
import os
import numpy as np
from datetime import datetime
from datetime import timedelta
import operator
import time
import copy
####################
# Global Variables

debug = 0
vernum = "0.1"
model_id = 0
#########


# Print version information and exit
def version():
    print "+----------------------------------------------------------------------+"
    print "| rcdetector.py Version "+ vernum +"                                           |"
    print "| This program is free software; you can redistribute it and/or modify |"
    print "| it under the terms of the GNU General Public License as published by |"
    print "| the Free Software Foundation; either version 2 of the License, or    |"
    print "| (at your option) any later version.                                  |"
    print "|                                                                      |"
    print "| Author: Garcia Sebastian, eldraco@gmail.com                          |"
    print "+----------------------------------------------------------------------+"
    print


# Print help information and exit:
def usage():
    print "+----------------------------------------------------------------------+"
    print "| rcdetector.py Version "+ vernum +"                                           |"
    print "| This program is free software; you can redistribute it and/or modify |"
    print "| it under the terms of the GNU General Public License as published by |"
    print "| the Free Software Foundation; either version 2 of the License, or    |"
    print "| (at your option) any later version.                                  |"
    print "|                                                                      |"
    print "| Author: Garcia Sebastian, eldraco@gmail.com                          |"
    print "+----------------------------------------------------------------------+"
    print "\nusage: %s <options>" % sys.argv[0]
    print "options:"
    print "  -h, --help                 Show this help message and exit"
    print "  -V, --version              Output version information and exit"
    print "  -D, --debug                Debug."
    print "  -f, --file                 Input netflow file to analize. If - is used, netflows are read from stdin. Remember to pass the header!"
    print "  -r, --training             Training mode. Read netflows and outputs one Markov Chain for each label in the folder 'MCModels'."
    print "  -e, --testing              Testing mode. Read netflows from -f file, Markov Chains from the folder 'MCModels', predicts for each tuple the chain (label) with more probability of generating it and it outputs a labeled netflow file."
    print "  -t, --threshold1           Threshold 1."
    print "  -y, --threshold2           Threshold 2."
    print
    sys.exit(1)


class model:
    """
    """
    def __init__(self):
        self.call_from = ''
        self.id = 0
        self.calls_to = {}
        self.total_seen = 0
        

    def add_call_to(self,call_to,date,label):
        """
        Count how many calls to each destination
        """
        try:
            t = self.calls_to[call_to]
            self.calls_to[call_to]['Amount'] += 1
            self.calls_to[call_to]['Dates'].append(date)
            self.calls_to[call_to]['Labels'].append(label)
            # On every new call also count how many call this number already did
            self.total_seen += 1

        except KeyError:
            self.calls_to[call_to] = {}
            self.calls_to[call_to]['Amount'] = 1
            self.calls_to[call_to]['Dates'] = []
            self.calls_to[call_to]['Labels'] = []
            self.calls_to[call_to]['Dates'].append(date)
            self.calls_to[call_to]['Labels'].append(label)

            # On every new call also count how many call this number already did
            self.total_seen += 1

        except Exception as inst:
            print 'Problem in add_call_to() in model class'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)






class callModels:
    """
    This class handles all the calls
    """
    def __init__(self):
        self.original_calls = {}
        self.call_id = 0
        # call_dict has the original call as key and the model inside
        self.call_dict = {}


    def output_metrics(self):
        """
        """
        try:
            TP = 0
            FN = 0
            TN = 0
            FP = 0
            for call_from in self.call_dict:
                model = self.call_dict[call_from]

                prediction = 'False'

                if model.len_calls_to >= self.threshold1 and model.total_amount >= self.threshold2:
                    #print 'Source phone:{}. Unique calls: {}. Total calls: {}. Common label: {}'.format(call_from, model.len_calls_to, model.total_amount, model.most_common_label)
                    prediction = 'True'


                if model.most_common_label == 'True' and prediction == 'True':
                    #TP
                    TP += 1
                elif model.most_common_label == 'True' and prediction == 'False':
                    #FN
                    FN += 1
                elif model.most_common_label == 'False' and prediction == 'False':
                    #TN
                    TN += 1
                elif model.most_common_label == 'False' and prediction == 'True':
                    #FP
                    FP += 1
                #if debug:
                #    print 'tp={}, fn={}, tn={}, fp={}'.format(tp,fn,tn,fp)
        
            try:
                TPR = ( TP) / float( TP + FN )
                TNR = ( TN ) / float( TN + FP )
                FPR = ( FP ) / float( TN + FP )
                FNR = ( FN ) / float( TP + FN )
                Precision = ( TP ) / float( TP + FP)
                Accuracy = ( TP + TN ) / float( TP + TN + FP + FN )
                ErrorRate = ( FN + FP ) / float( TP + TN + FP + FN )
                beta = 1.0
                fmeasure1 = ( ( (beta * beta) + 1 ) * Precision * TPR  ) / float( ( beta * beta * Precision ) + TPR )
                if debug:
                    print 'tp={}, fn={}, tn={}, fp={}. TPR:{}, TNR: {}, FPR: {}, FNR: {}, Prec: {}, Accu: {}, ErRate: {}, FM: {}'.format(TP,FN,TN,FP,TPR,TNR,FPR,FNR,Precision,Accuracy,ErrorRate,fmeasure1)
                return (TP,FN,TN,FP,TPR,TNR,FPR,FNR,Precision,Accuracy,ErrorRate,fmeasure1)
            except ZeroDivisionError:
                print 'No enough data'
                sys.exit(-1)
                return False



        except Exception as inst:
            print 'Problem in compute_features()'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def compute_features(self):
        """
        """
        try:
            for call_from in self.call_dict:
                model = self.call_dict[call_from]
                calls_to_dict = model.calls_to
                total_amount = 0
                total_labels = 0
                for dest_call in calls_to_dict:
                    amount = calls_to_dict[dest_call]['Amount']
                    total_amount += amount
                for dest_call in calls_to_dict:
                    labels = calls_to_dict[dest_call]['Labels']

                    #try:
                    #    if labels.index('True') > 0 and labels.index('False') > 0:
                    #        print 'This phone has more than 1 label: {}. Robocalls: {}, Normals: {}'.format(call_from)
                    #except ValueError:
                    #    pass

                    most_common_label = max(set(labels), key=labels.count)
                len_calls_to = len(calls_to_dict)

                # Store the features
                model.len_calls_to = len_calls_to
                model.total_amount = total_amount
                model.most_common_label = most_common_label
        except Exception as inst:
            print 'Problem in compute_features()'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def print_calls(self):
        """
        """
        try:
            for call_from in self.call_dict:
                model = self.call_dict[call_from]
                if model.len_calls_to >= self.threshold1 and model.total_amount >= self.threshold2:
                    if debug > 1:
                        print 'Source phone: {} . Unique calls: {}. Total calls: {}. Most Common label: {}'.format(call_from, model.len_calls_to, model.total_amount, model.most_common_label)
            #print 'Amount of unique source phone numbers:{}'.format(len(self.call_dict))

        except Exception as inst:
            print 'Problem in compute_features()'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)




    def get_model_for_call(self,call):
        """                   
        the calls are ordered by from_call
        """
        global model_id
        try:
            temp_model = self.call_dict[call]
            # No

        except KeyError:
            # Yes
            temp_model = model()           
            temp_model.call_from = call       
            temp_model.id = model_id       
            model_id += 1
            self.call_dict[call] = temp_model
        return temp_model



    def add_call(self,callArray):
        """
        Receives a call and adds it to the model
        """
        try:
            global debug

            if debug > 5:
                print '\nAdding call: {} '.format(callArray)

            # Just in case they are not in the file
            runtime = -1
            srcbytes = -1

            for col in callArray:
                if 'TO' in col.keys()[0]:
                    call_to = str(col.values()[0])
                elif 'FROM' in col.keys()[0]:
                    call_from = str(col.values()[0])
                elif 'DATE' in col.keys()[0]:
                    date = str(col.values()[0])
                elif 'LIKELY' in col.keys()[0]:
                    if str(col.values()[0]) == 'X':
                        label = 'True' 
                    else:
                        label = 'False' 

            if debug > 5:
                print 'To:{}, from:{}, date:{}, likely:{}'.format(call_to, call_from, date, label)

            # Get the model
            model = self.get_model_for_call(call_from)

            # Assign to the model, the threshold of the stateModel
            model.add_call_to(call_to,date,label)

            # Add this model and tuple to the list of tuples in this stateModel
            self.call_dict[call_from] = model


        except Exception as inst:
            print 'Problem in add_call()'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)




    def process_calls(self,rbFile):
        """
        This function takes the calls file and parse it. 
        """
        try:
            global debug

            if debug:
                print 'Processing the rbcall file {0}'.format(rbFile)


            # Read the file and parse the input
            try:
                # From stdin or file?
                    f = open(rbFile,'r')
            except Exception as inst:
                print 'Some problem opening the input call file. In process_calls()'
                print type(inst)     # the exception instance
                print inst.args      # arguments stored in .args
                print inst           # __str__ allows args to printed directly
                sys.exit(-1)


            # Just to monitor how many lines we read
            self.call_id = 0
            line = f.readline().strip()
            self.call_id += 1

            # Is the first line the labels?
            if not 'TO' in line:
                print 'Warning! It seems that you miss the headers line. Please provide it as the first line in the archive.'
                sys.exit(-1)

            columnDict = {}
            templateColumnArray = []
            columnArray = []
            columnNames = line.split(',')

            if debug > 4:
                print 'Columns names: {0}'.format(columnNames)

            for col in columnNames:
                columnDict[col] = ""
                templateColumnArray.append(columnDict)
                columnDict = {}

            columnArray = templateColumnArray


            # Read the second line to start processing
            line = f.readline().strip()
            self.call_id += 1

            # To store the netflows we should put the data in a dict
            self.original_calls[self.call_id] = line

            while (line):
                if debug > 5:
                    print 'Call line: {0}'.format(line)

                # Parse the columns
                columnValues = line.split(',')

                if debug > 5:
                    print columnValues

                i = 0
                for col in columnValues:
                    tempDict = columnArray[i]
                    tempDictName = tempDict.keys()[0]
                    tempDict[tempDictName] = col
                    columnArray[i] = tempDict
                    i += 1

                if debug > 5:
                    print columnArray

                # Add the call to the model.
                self.add_call(columnArray)

                
                # Go back to the empty array
                columnArray = templateColumnArray

                line = f.readline().strip()
                self.call_id += 1
                self.original_calls[self.call_id] = line

            # End while

            if debug:
                print 'Amount of lines read: {0}'.format(self.call_id)

        except Exception as inst:
            print 'Problem in process_calls()'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            sys.exit(-1)








def main():
    try:
        global debug

        rbFile = ""
        training_mode = False
        testing_mode = False
        threshold1 = -1
        threshold2 = -1

        opts, args = getopt.getopt(sys.argv[1:], "VD:hf:ret:y:", ["help","version","debug=","file=","training","testing","threshold1=","threshold2="])
    except getopt.GetoptError: usage()

    for opt, arg in opts:
        if opt in ("-h", "--help"): usage()
        if opt in ("-V", "--version"): version();sys.exit(-1)
        if opt in ("-D", "--debug"): debug = int(arg)
        if opt in ("-f", "--file"): rbFile = str(arg)
        if opt in ("-r", "--training"): training_mode = True
        if opt in ("-e", "--testing"): testing_mode = True
        if opt in ("-t", "--threshold1"): threshold1 = float(arg)
        if opt in ("-y", "--threshold2"): threshold2 = float(arg)
    try:
        try:
            # Create an instance
            callModel = callModels()
            callModel.threshold1 = threshold1
            callModel.threshold2 = threshold2
            if rbFile == "":
                usage()
                sys.exit(1)

            elif rbFile != "":
                # Process calls
                callModel.process_calls(rbFile)

                # Training
                if training_mode and not testing_mode:
                    if debug:
                        print 'Training...'
                     
                    callModel.compute_features()
                    callModel.print_calls()

                    bestFM = -1
                    bestt1 = 0
                    bestt2 = 0
                    best_results = ()
                    for callModel.threshold1 in range(1,300):
                        for callModel.threshold2 in range(1,300):
                            if debug:
                                print 't1= {}, t2= {}'.format(callModel.threshold1, callModel.threshold2)

                            (TP,FN,TN,FP,TPR,TNR,FPR,FNR,Precision,Accuracy,ErrorRate,fmeasure1) = callModel.output_metrics()

                            if fmeasure1 >= bestFM:
                                bestFM = fmeasure1
                                bestt1 = copy.deepcopy(callModel.threshold1)
                                bestt2 = copy.deepcopy(callModel.threshold2)
                                best_results = copy.deepcopy((TP,FN,TN,FP,TPR,TNR,FPR,FNR,Precision,Accuracy,ErrorRate,fmeasure1))
                                print 'Get a better fm {}, with t1: {} and t2: {}'.format(fmeasure1,bestt1, bestt2)

                    print 'Best t1: {}, t2: {}'.format(bestt1, bestt2)
                    (TP,FN,TN,FP,TPR,TNR,FPR,FNR,Precision,Accuracy,ErrorRate,fmeasure1) = best_results
                    print 'TP:{}, FN: {}, TN: {}, FP: {}, TPR: {}, TNR: {}, FPR: {}, FNR: {}, Pre: {}, Accu: {}, ERate: {}, FM: {}'.format(TP,FN,TN,FP,TPR,TNR,FPR,FNR,Precision,Accuracy,ErrorRate,fmeasure1)


                # Testing
                elif not training_mode and testing_mode:
                    if debug:
                        print 'Testing...'
                    callModel.compute_features()
                    callModel.print_calls()
                    (TP,FN,TN,FP,TPR,TNR,FPR,FNR,Precision,Accuracy,ErrorRate,fmeasure1) = callModel.output_metrics()
            else:
                    usage()
                    sys.exit(1)
        except Exception, e:
                print "misc. exception (runtime error from user callback?):", e
        except KeyboardInterrupt:
                sys.exit(1)


    except KeyboardInterrupt:
        # CTRL-C pretty handling.
        print "Keyboard Interruption!. Exiting."
        sys.exit(1)


if __name__ == '__main__':
    main()

# Idea, robocalls call a lot of numbers in percentage
# Robocalls calls every 5 minutes (when is idle?) : Time diff between calls
# Robocalls calls several different numbers, not 1 or 2 only : Amount of destination numbers

# Broken dataset? same source phone and is both robocall and not???????
#12015465505,14042766806,4/1/2014 13:44,X
#12015465505,18623072643,4/1/2014 17:50,
#12015465505,12135197983,4/1/2014 23:31,X
#12015465505,17329078661,4/2/2014 12:27,X
#12015465505,12135197983,4/2/2014 19:50,X
#12015465505,19086039310,4/3/2014 18:46,
#12015465505,16782032781,4/4/2014 18:52,X
#12015465505,17324125688,4/4/2014 20:24,

